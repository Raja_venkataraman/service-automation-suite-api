{
  "map": {
    "lastName": "TesterLastName",
    "byPhone": "true",
    "optIn": "true",
    "title": "Ms",
    "locale": "GB",
    "titleCode": "0018",
    "firstName": "testFirstName",
    "password": "passw0rd",
    "bySupport": "true",
    "byEmail": "true",
    "preferredName": "Tester",
    "byPost": "true",
    "email": "LeapingTester_test237@dyson.com",
    "bySMS": "true"
  }
}