
Feature: Connected-IntegrationServices Suite - Release Smoke

  Scenario: CreateUser
    Given a rest api "BaseUrl" with relaxed HTTPS Validation
    And a header
      | accept       | application/json |
      | Content-Type | application/json |
      | X-version    | 1.0              |
    And the request body is "NewCreateUser.data1"
    When the system requests POST "/apiman-gateway/dyson/user-registration/1.0/us/"
    Then trace out response and attach to report
    And the response code is 200
    And store the response body
      | CreateUserUpdate |


  Scenario: Request Reset Password
    Given a rest api "InternalServiceEndPoint" with relaxed HTTPS Validation
    And a header
      | Content-Type | application/form-data |
    And form parameters from inputs file
      | email | <<NewCreateUser.data1>> |
#    And form parameters
#      | Email | LeapingTester_test237@dyson.com |
    #    try to pass this email from 1st scenario createuserupdate.json
    When the system requests POST "/apiman-gateway/internal/reset-password/1.0?country=GB"
    Then trace out response and attach to report
    And the response code is 204
