@wip-usha
  #everything done
Feature: Connected-IntegrationServices Suite - Release Smoke

  Scenario: Create Account
    Given a rest api "InternalServiceEndPoint" with relaxed HTTPS Validation
    And a header
      | Content-Type | application/x-www-form-urlencoded |
    And path parameters
      | culture | en-GB |
      | country | GB |
    And form parameters
      | Honorific | Ms |
      | FirstName | testFirstName- |
      | LastName | TesterLastName- |
      | Email | LeapTester_test148@dyson.com |
      | Password | Passw0rd |
      | ContactPreferencesEmail | true |
      | ContactPreferencesPhone | true |
      | ContactPreferencesMail | true |
      | ContactPreferencesTextMessage | true |
    When the system requests POST "/apiman-gateway/internal/register/1.0?culture={culture}&country={country}"
    Then trace out response and attach to report
    And the response code is 200
    And store the response body
      | RegisterProduct |


  Scenario: Register Product
    Given a rest api "InternalServiceEndPoint" with relaxed HTTPS Validation
    And a header
      | Content-Type | application/x-www-form-urlencoded |
    And form parameters from outputs file
      | PurchaseDate | 2019-06-07 |
      | Serial | ZW8-GB-GBT1234A |
      | dysonAccountGuid | <<RegisterProduct.data1>> |
    When the system requests POST "/apiman-gateway/internal/register-product/1.0?country=GB"
    Then trace out response and attach to report
    And the response code is 204


