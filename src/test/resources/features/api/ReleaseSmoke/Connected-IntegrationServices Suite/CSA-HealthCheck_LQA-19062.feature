

Feature: Connected-IntegrationServices Suite - Release Smoke

Scenario: CSA-HealthCheck_LQA-19062
Given a rest api "InternalServiceEndPoint" with relaxed HTTPS Validation
When the system requests GET "/apiman-gateway/internal/health-check/1.0"
And the response code is 200