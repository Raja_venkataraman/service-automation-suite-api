
Feature: Connected-IntegrationServices Suite - Release Smoke

  Scenario: Create Account
    Given a rest api "InternalServiceEndPoint" with relaxed HTTPS Validation
    And a header
      | Content-Type | application/x-www-form-urlencoded |
    And path parameters
      | culture | en-GB |
      | country | GB |
    And form parameters
      | Honorific | Ms |
      | FirstName | testFirstName- |
      | LastName | TesterLastName- |
      | Email | LeapTester_test134@dyson.com |
      | Password | Passw0rd |
      | ContactPreferencesEmail | true |
      | ContactPreferencesPhone | true |
      | ContactPreferencesMail | true |
      | ContactPreferencesTextMessage | true |
    When the system requests POST "/apiman-gateway/internal/register/1.0?culture={culture}&country={country}"
    Then trace out response and attach to report
    And the response code is 200
#    And "Dyson Account Guid" id exists in the response

#
#  Scenario: Attempt to register email already in use
#    Given a rest api "InternalServiceEndPoint" with relaxed HTTPS Validation
#    And a header
#      | Content-Type | application/x-www-form-urlencoded |
#    And "form parameters" from "NewUserRegistration.data1"
#      | Honorific | Ms |
#      | FirstName | testFirstName- |
#      | LastName | TesterLastName- |
#      | Email | LeapTester_test130@dyson.com |
#      | Password | Passw0rd |
#      | ContactPreferencesEmail | true |
#      | ContactPreferencesPhone | true |
#      | ContactPreferencesMail | true |
#      | ContactPreferencesTextMessage | true |
#    When the system requests POST "/apiman-gateway/internal/register/1.0?culture=en-GB&country=GB"
#    Then trace out response and attach to report
#    And the response code is 400
#    And "EMAIL_ALREADY_REGISTERED" id exists in the response

