package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class UploadHelper {

    /**
     * get upload dir path
     * @return upload dir path
     */
    public static File getUploadDir (){
        return new File("src/test/resources/testdata/ui/upload");
    }

    /**
     * create a file to upload
     * @return File
     */
    public static File genFile(){
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        String name = String.format("upload_%d.txt", now.toEpochSecond(ZoneOffset.UTC));
        File newFile = new File(getUploadDir(), name);
        String data = String.format("This file was upload at %s", dateTimeFormatter.format(now));
        try (FileWriter writer = new FileWriter(newFile)){
            writer.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFile;
    }
}
