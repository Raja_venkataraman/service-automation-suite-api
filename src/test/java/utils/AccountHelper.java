package utils;

import model.AccountModel;

import java.io.File;
import java.io.IOException;

public class AccountHelper {

    public static AccountModel getCredentials(String accountName) throws IOException {
        String fileName = String.format("src/test/resources/testdata/ui/accounts/%s.yml", accountName);
        File file = new File(fileName);
        return YamlHelper.readValue(file, AccountModel.class);
    }
}
