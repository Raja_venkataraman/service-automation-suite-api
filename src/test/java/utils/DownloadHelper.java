package utils;


import automation.library.selenium.exec.driver.factory.DriverFactory;
import com.google.common.collect.Iterables;
import io.cucumber.java8.Fi;
import org.apache.commons.io.FileUtils;
import org.aspectj.util.FileUtil;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Collection;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class DownloadHelper {

    private File downloadDir = new File(System.getProperty("user.home")+"/Downloads/");
    private Collection<File> currentFiles;
    private Pattern fileType;

    /**
     * Create a DownloadHelper object that liiks at files mating the regex
     * @param fileType regex
     */
    public DownloadHelper(String fileType){
        this.fileType = Pattern.compile(fileType);
    }

    /**
     * Get a copy of all current files matching the pattern
     */
    public void snapshotDir() {
        currentFiles = getCurrentFiles();

    }

    /**
     * Get a collection of current files
     * @return Collection<File>
     */
    private Collection<File> getCurrentFiles() {
        Collection<File> files = FileUtils.listFiles(downloadDir, null, false);
        files.removeIf(filterByPatten());
        return files;
    }

    /**
     * Get the filter By Patten function
     * @return function
     */
    private Predicate<File> filterByPatten() {
        return file -> !fileType.matcher(file.getName()).matches();
    }

    /**
     * Wait For files to download the match pattern
     * @throws TimeoutException
     */
    public void waitForFiles() throws TimeoutException {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), 60);
        wait.until(driver -> {
            Collection<File> files = getCurrentFiles();
            return files.size() > currentFiles.size();
        });
    }

    /**
     * Get collection of new files after snapshot
     * @return Collection<File>
     */
    public Collection<File> newFiles(){
        Collection<File> files = getCurrentFiles();
        files.removeAll(currentFiles);
        return files;
    }


    
    public static String dupName(String name){
        String[] split = name.split("\\.");
        StringJoiner sj = new StringJoiner(".");
        for (int i = 0; i < split.length ; i++) {
            if (split.length > 1 && i == (split.length - 2)){
                sj.add(split[i] + "( \\(\\d+\\))?");
            }else {
                sj.add(split[i]);
            }
        }
        return sj.toString();
    }


}
