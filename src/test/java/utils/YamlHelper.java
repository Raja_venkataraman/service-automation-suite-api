package utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;

public class YamlHelper {

    /**
     * Setup ObjectMapper to work with yaml
     * @return ObjectMapper
     */
    public static ObjectMapper getMapper(){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.findAndRegisterModules();
        return mapper;
    }

    /**
     * Read data from file
     * @param src file pathe
     * @param valueType class
     * @return class with data
     * @throws IOException
     */
    public static <T> T readValue(File src, Class<T> valueType) throws IOException {
        return getMapper().readValue(src, valueType);
    }

    public static void writeValue(File src, Object value) throws IOException {
        getMapper().writeValue(src, value);
    }

}
