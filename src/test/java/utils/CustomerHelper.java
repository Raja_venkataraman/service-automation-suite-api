package utils;

import model.CustomerModel;

import java.io.File;
import java.io.IOException;

public class CustomerHelper {

    public static CustomerModel getCustomer(String accountName) throws IOException {
        String fileName = String.format("src/test/resources/testdata/ui/customers/%s.yml", accountName);
        File file = new File(fileName);
        return YamlHelper.readValue(file, CustomerModel.class);
    }
}
