package utils;

import model.CaseModel;

import java.io.File;
import java.io.IOException;

public class CaseHelper {

    public static CaseModel getCase(String accountName) throws IOException {
        String fileName = String.format("src/test/resources/testdata/ui/cases/%s.yml", accountName);
        File file = new File(fileName);
        return YamlHelper.readValue(file, CaseModel.class);
    }
}
