package utils;

import model.CaseModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class QuestionnaireHelper {

    public static OrderProperties getQuestionnaire(String questionnaire) throws IOException {
        String fileName = String.format("src/test/resources/testdata/ui/questionnaire/%s.properties", questionnaire);
        OrderProperties properties = new OrderProperties();
        FileInputStream file = new FileInputStream(fileName);
        properties.load(file);

        return properties;
    }
}
