package utils;

import automation.library.common.TestContext;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class UniqueValue {

    public static String uniqueValue(String value){
        return String.format("%s.%s",value, uuid());
    }

    public static String uniqueValueFromDate(String value){
        return String.format("%s%s",value, date());
    }

    public static String date(){
        Map<String, Object> testdata = TestContext.getInstance().testdata();
        String uuidDate;
        if (testdata.containsKey("uuidDate")){
            uuidDate = (String) testdata.get("uuidDate");
        }else {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddhhmmss");
            uuidDate = simpleDateFormat.format(date);
            TestContext.getInstance().testdataPut("uuidDate", uuidDate);
        }
        return uuidDate;
    }

    public static String uuid(){
        Map<String, Object> testdata = TestContext.getInstance().testdata();
        String uuidValue;
        if (testdata.containsKey("uuid")){
            uuidValue = (String) testdata.get("uuid");
        }else {
            uuidValue = UUID.randomUUID().toString();
            TestContext.getInstance().testdataPut("uuid", uuidValue);
        }
        return uuidValue;
    }
}
