package utils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Templates data using specific keywords with unique values.
 * This class is useful to generate unique test data.
 */
public class DataTemplater {

    private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private LocalDateTime date = LocalDateTime.now(ZoneOffset.UTC);


    /**
     * Templates strings where they have a [GenerateDateTime] keyword with a
     * date time string
     * <p>
     * Default date format: "yyyy-MM-dd-HH:mm:ss"
     *
     * @param value i.e. "test-[GenerateDateTime]@test.com"
     * @return templated string i.e. "testdxp-2019-12-15-21:47:27@dyson.com"
     */
    public String template(String value) {
        String templatedValue = value;
        if (value.contains("[GenerateDateTime]")) {
            templatedValue = templatedValue.replace("[GenerateDateTime]", date.format(dateFormat));
        }
        return templatedValue;
    }

    /**
     * Gets date string. This can be customised with different date formats, as well as
     * offsetting the date with number of days forward/backwards
     *
     * @param format     String i.e. yyyy-MM-dd HH:mm:ss = 2019-12-01 09:28:01
     * @param daysOffset int i.e. -3 (3 days in the past), 8 (8 days in the future)
     * @return formatted date-time string
     */
    public String getDateString(String format, int daysOffset) {

        LocalDateTime customDate = date.plusDays(daysOffset);

        return customDate.format(DateTimeFormatter.ofPattern(format));
    }
}
