package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class DateHelper {

    private static String getDateFormat(String territory) throws IOException {
        String fileName = "src/test/resources/testdata/ui/date_format.properties";
        Properties properties = new Properties();
        FileInputStream file = new FileInputStream(fileName);
        properties.load(file);

        return properties.getProperty(territory);
    }

    public static DateTimeFormatter formatDateTime(String territory) throws IOException {
        String format = getDateFormat(territory + "_date_time");
        return DateTimeFormatter.ofPattern(format);
    }

    public static DateTimeFormatter formatDate(String territory) throws IOException {
        String format = getDateFormat(territory + "_date");
        return DateTimeFormatter.ofPattern(format);
    }

    public static DateTimeFormatter formatTime(String territory) throws IOException {
        String format = getDateFormat(territory + "_time");
        return DateTimeFormatter.ofPattern(format);
    }
}
