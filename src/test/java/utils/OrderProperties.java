package utils;

import java.util.*;

public class OrderProperties extends Properties {

    private final HashSet<Object> keys = new LinkedHashSet<Object>();

    public OrderProperties() {
    }

    public List<Object> orderedKeys() {
        return Collections.list(keys());
    }

    public Enumeration<Object> keys() {
        return Collections.<Object>enumeration(keys);
    }

    public Object put(Object key, Object value) {
        keys.add(key);
        return super.put(key, value);
    }
}
