package utils;


import automation.library.api.core.RestContext;
import io.restassured.http.Header;
import io.restassured.http.Headers;

import java.util.HashMap;
import java.util.Map;

public class APIDownloadHelper {

    public APIDownloadHelper() {

    }

    public static Map<String, String> getContentDisposition(RestContext restContext) {
        Headers headers = restContext.getRestData().getResponse().getHeaders();
        Map<String, String> contentDisposition = new HashMap<>();
        for (Header header : headers) {
            if (header.getName().equals("Content-Disposition")) {
                String[] rawContentDisposition = header.getValue().split("; ");

                for (String paramater :
                        rawContentDisposition) {
                    String[] keyVals = paramater.split("=");
                    if (keyVals.length == 1) {
                        contentDisposition.put("type", rawContentDisposition[0]);
                    } else {
                        String key = keyVals[0];
                        String value = keyVals[1];
                        contentDisposition.put(key, value);
                    }
                }
            }
        }
        return contentDisposition;
    }
}
