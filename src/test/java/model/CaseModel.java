package model;

public class CaseModel {

    private String id;
    private String caseType;
    private String crmRefNo;
    private String productSerialNo;
    private String purchasedDate;
    private String productModel;
    private String productVariant;
    private String description;
    private String batchCode;
    private String caseSeverity;

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    private String diagnosis;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getProductSerialNo() {
        return productSerialNo;
    }

    public void setProductSerialNo(String productSerialNo) {
        this.productSerialNo = productSerialNo;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductVariant() {
        return productVariant;
    }

    public void setProductVariant(String productVariant) {
        this.productVariant = productVariant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCrmRefNo() {
        return crmRefNo;
    }

    public void setCrmRefNo(String crmRefNo) {
        this.crmRefNo = crmRefNo;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getCaseSeverity() {
        return caseSeverity;
    }

    public void setCaseSeverity(String caseSeverity) {
        this.caseSeverity = caseSeverity;
    }

    /**
     * Get detail by key
     * @param key
     * @return value
     * @throws Exception
     */
    public String getDetail(String key) throws Exception {
        switch (key){
            case "description":
                return description;
            default:
                String wrongKey = String.format("Key %s is not valid", key);
                throw new Exception(wrongKey);
        }
    }
}
