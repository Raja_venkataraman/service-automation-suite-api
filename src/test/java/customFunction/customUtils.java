package customFunction;

import automation.library.api.core.RestAssuredHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.qameta.allure.Allure;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import static automation.library.common.FileHelper.getJSONArray;
import static automation.library.common.FileHelper.getJSONData;

public class customUtils {

    private static FileWriter inputFile, outputFile;

    public int listOfRecords = 0;

    public customUtils() {

    }

    public static void setBodyInputId(String data, RequestSpecification restContext) {
        RestAssuredHelper.setBody(restContext, data);
    }

    public static void setBodyInput(String data, RequestSpecification restContext) {
        String[] str = data.split("\\.");
        String baseTestData = "src/test/resources/testdata/inputs/" + str[0] + ".json";
        System.out.println(baseTestData);
        String baseDataSet = str[1];
        JSONObject jsonTest = getJSONData(baseTestData, baseDataSet);
        String jsonData = jsonTest != null ? jsonTest.toString() : "null";
        RestAssuredHelper.setBody(restContext, jsonData);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println("Request Body");
        System.out.println("============================================================");
        System.out.println(gson.toJson(jsonTest));
        Allure.addAttachment("Request Body", gson.toJson(jsonTest));
    }

    public static void recordResponse(String response) {
        Allure.getLifecycle().addAttachment(
                "Response",
                "text/plain",
                "txt",
                response.getBytes());
    }

    /**
     * Convert a JSON string to pretty print version
     *
     * @param jsonString to format
     * @return pretty print JSON string
     */
    public static String toPrettyFormat(String jsonString) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonString).getAsJsonObject();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJson = gson.toJson(json);

        return prettyJson;
    }

    /**
     * Saves Requests and Response data to Input/Output files.
     *
     * @param response         data to save
     * @param location         "inputs"/"outputs"
     * @param responseFileName filename to save in
     */
    public static void saveResponse(String response, String location, String responseFileName) {
        try {
            String filepath = "src/test/resources/testdata/" + location + "/" + responseFileName + ".json";
            outputFile = new FileWriter(filepath);
            if (response.contains("data1")) {
                outputFile.write(toPrettyFormat(response));
            } else {
                String rawResponse = String.format("{\"data1\":%s}", response);
                outputFile.write(toPrettyFormat(rawResponse));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputFile.flush();
                outputFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public static void recordResponse(int response) {
        Allure.getLifecycle().addAttachment(
                "Response",
                "text/plain",
                "txt",
                String.valueOf(response).getBytes());
    }

    public static String getNumberOfRecordsResponse(String response, int data) {
        JSONObject jsonString = new JSONObject(response);
        int numberOfRecords = jsonString.getJSONArray("data").length();
        String numberOfRecordsMatch = "";
        if (numberOfRecords <= data) {
            numberOfRecordsMatch = "Number of records is at most " + data;
        } else {
            numberOfRecordsMatch = "Number of records is greater " + data;
        }
        return numberOfRecordsMatch;
    }

    public static int getValueFromJSONFile(String fileName, String Id) {
        int idValue = 0;
        String response = "";
        String[] filename = fileName.replace("<<", "").replace(">>", "").split("\\.");
        String baseDataSet = filename[1];
        String baseTestData = "src/test/resources/testdata/outputs/" + filename[0] + ".json";
        JSONObject jsonTest = getJSONData(baseTestData, baseDataSet);
        String jsonData = jsonTest != null ? jsonTest.toString() : "null";
        String str = jsonData.replace("[", "");
        String modifiedStr = str.replace("]", "");
        JSONObject jsonString = new JSONObject(modifiedStr);
        for (int i = 0; i < jsonString.length(); ++i) {
            idValue = (int) jsonString.get(Id);
        }
        return idValue;
    }

    /**
     * Get given Id from input/output JSON
     *
     * @param dir:      "inputs"/"outputs"
     * @param fileName: fileName to get Id from
     * @param Id:       name of Id
     * @return Id
     */
    public static String getValueFromJSONFile(String dir, String fileName, String Id) {
        String idValue = "";
        String[] filename = fileName.replace("<<", "").replace(">>", "").split("\\.");
        String baseDataSet = filename[1];
        String baseTestData = "src/test/resources/testdata/" + dir + "/" + filename[0] + ".json";
        JSONObject jsonTest = getJSONData(baseTestData, baseDataSet);
        String jsonData = jsonTest != null ? jsonTest.toString() : "null";
        String str = jsonData.replace("[", "");
        String modifiedStr = str.replace("]", "");
        JSONObject jsonString = new JSONObject(modifiedStr);
        for (int i = 0; i < jsonString.length(); ++i) {
            idValue = jsonString.get(Id).toString();
        }
        return idValue;
    }

    public static String getIdNameResponse(String fileName, String Id) {
        String idValue = "";
        String[] filename = fileName.replace("<<", "").replace(">>", "").split("\\.");
        String baseDataSet = filename[1];
        String baseTestData = "src/test/resources/testdata/outputs/" + filename[0] + ".json";
        JSONObject jsonTest = getJSONData(baseTestData, baseDataSet);
        String jsonData = jsonTest.toString();
        String str = jsonData.replace("[", "");
        String modifiedStr = str.replace("]", "");
        JSONObject jsonString = new JSONObject(modifiedStr);
        for (int i = 0; i < jsonString.length(); ++i) {
            idValue = (String) jsonString.get(Id);
        }
        return idValue;
    }

    public static int getIdFromResponse(String response, String Id) {
        int idValue = 0;
        String str = response.replace("[", "");
        String modifiedStr = str.replace("]", "");
        JSONObject jsonString = new JSONObject(modifiedStr);
        for (int i = 0; i < jsonString.length(); ++i) {
            idValue = (int) jsonString.get(Id);
        }
        return idValue;
    }

    public static void responseBodyContain(String mode, String filename, String dataset, String responseString) throws JSONException {

        JSONCompareMode compareMode = (mode.equals("strictly") ? JSONCompareMode.STRICT : JSONCompareMode.LENIENT);

        Map<String, String> map;
        JSONObject obj = getJSONData("src/test/resources/testdata/outputs/" + filename + ".json");
        //JSONArray or JSONObject casting to preserve data ordering
        JSONArray inputArray = new JSONArray();
        inputArray.put(obj.get(dataset));
        JSONObject responseJsonObject = null;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println("Expected");
        System.out.println("============================================================");
        System.out.println(gson.toJson(obj.get(dataset)));
        Allure.addAttachment("Expected", gson.toJson(obj.get(dataset)));

        if (responseString.startsWith("[")) {
            JSONArray responseArray = null;
            responseArray = new JSONArray(responseString);
            Allure.addAttachment("Actual", gson.toJson(responseArray));
            if (obj.get(dataset).toString().startsWith("[")) {
                JSONAssert.assertEquals("Expected response body differs from actual array", (JSONArray) obj.get(dataset), responseArray, compareMode);
            } else {
                JSONAssert.assertEquals("Expected response body differs from actual array", inputArray, responseArray, compareMode);
            }
        } else if (responseString.contains("data")) {
            responseJsonObject = new JSONObject(responseString);
            Allure.addAttachment("Actual", gson.toJson(responseJsonObject));
            JSONAssert.assertEquals("Expected response body differs from actual", (JSONObject) obj.get(dataset), (JSONObject) responseJsonObject.get("data"), compareMode);
        } else {
            responseJsonObject = new JSONObject(responseString);
            Allure.addAttachment("Actual", gson.toJson(responseJsonObject));
            JSONAssert.assertEquals("Expected response body differs from actual", (JSONObject) obj.get(dataset), (JSONObject) responseJsonObject, compareMode);
        }
    }

    public static String getAuthResponse(String Id) {
        String idValue = "";
        String baseDataSet = "data1";
        String response = "";
        String baseTestData = "src/test/resources/testdata/outputs/newAuthToken.json";
        JSONObject jsonTest = getJSONData(baseTestData, baseDataSet);
        String jsonData = jsonTest != null ? jsonTest.toString() : "null";
        String str = jsonData.replace("[", "");
        String modifiedStr = str.replace("]", "");
        JSONObject jsonString = new JSONObject(modifiedStr);
        for (int i = 0; i < jsonString.length(); ++i) {
            idValue = (String) jsonString.get(Id);
        }
        return idValue;
    }

    /**+
     *
     * @param fileName validInputValues.json
     * @param parameterValue field values such as customerId, caseId
     * @return fieldValue
     */
    public static int getFieldFromInputFile(String fileName, String parameterValue) {
        int fieldValue = 0;
        String response = "";
        String[] filename = fileName.replace("<<", "").replace(">>", "").split("\\.");
        String baseDataSet = filename[1];
        String baseTestData = "src/test/resources/testdata/inputs/" + filename[0] + ".json";
        JSONObject jsonTest = getJSONData(baseTestData, baseDataSet);
        String jsonData = jsonTest != null ? jsonTest.toString() : "null";
        String str = jsonData.replace("[", "");
        String modifiedStr = str.replace("]", "");
        JSONObject jsonString = new JSONObject(modifiedStr);
        for (int i = 0; i < jsonString.length(); ++i) {
            fieldValue = (int) jsonString.get(parameterValue);
        }
        return fieldValue;
    }

    public static String createJsonArray(String testData) throws FileNotFoundException, JSONException {
        String jsonData = null;
        JSONArray jsonTest = null;

        if (testData.substring(0, 2).equalsIgnoreCase("<<")) {
            String[] str = testData.replace("<<", "").replace(">>", "").split("\\.");
            String testDataFile = str[0];
            String testDataSet = str[1];
            String path = "src/test/resources/testdata/inputs/" + testDataFile + ".json";
            jsonTest = getJSONArray(path, testDataSet);
        } else {
            jsonTest = new JSONArray(testData);
        }

        assert jsonTest != null;
        jsonData = jsonTest.toString();

        return jsonData;
    }
}
