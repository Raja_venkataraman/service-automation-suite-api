package customFunction;

import automation.library.api.core.RestAssuredHelper;
import io.restassured.specification.RequestSpecification;

import java.io.File;

public class RestAssuredHelperExtension extends RestAssuredHelper {

    public static RequestSpecification setMultiPartForm(RequestSpecification request, String fileName) {
        String baseFilePath = "src/test/resources/testdata/inputs/";
        String filePath = baseFilePath + fileName;
        File file = new File(filePath);
        return request.multiPart(file);
    }

}
