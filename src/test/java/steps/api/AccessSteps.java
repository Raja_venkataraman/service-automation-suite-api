package steps.api;

import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import automation.library.common.Property;
import automation.library.common.TestContext;
import automation.library.cucumber.core.Constants;
import io.cucumber.java.en.Given;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;

public class AccessSteps {
    JSONObject jsonTest = null;

    RestContext restContext;
    RequestSpecification request = given();

    public AccessSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    private String api = null;
    private String path = null;
    private String method = null;

    public int idValue = 0;
    public String idNameValue = "";


    @Given("^set client secret$")
    public void setClientSecret() {
        String client_secret = Property.getVariable("CLIENT_SECRET");
        RestAssuredHelper.setParam(restContext.getRestData().getRequest(), "form parameters", "client_secret", client_secret);
    }

    @Given("^set client id as: \"(.*)\"$")
    public void setClientID(String clientIDName) {
        String envPropPath = Constants.ENVIRONMENTPATH + Property.getVariable("cukes.env") + ".properties";
        String clientID = Property.getProperty(envPropPath, clientIDName);
        TestContext.getInstance().testdataPut("clientIDName", clientIDName);
        RestAssuredHelper.setParam(restContext.getRestData().getRequest(),
                "form parameters",
                "client_id",
                clientID);
    }

    @Given("^set client secret invalid$")
    public void setClientSecretInvalid() {
        String invalid_client_secret = Property.getProperty(Constants.ENVIRONMENTPATH + Property.getVariable("cukes.env") + ".properties", "invalid_client_secret");
        String scope = Property.getProperty(Constants.ENVIRONMENTPATH + Property.getVariable("cukes.env") + ".properties", "scope");
        RestAssuredHelper.setParam(restContext.getRestData().getRequest(), "form parameters", "client_secret", invalid_client_secret);
        RestAssuredHelper.setParam(restContext.getRestData().getRequest(), "form parameters", "scope", scope);

    }

}
