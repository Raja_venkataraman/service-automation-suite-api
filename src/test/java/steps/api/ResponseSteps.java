package steps.api;

import automation.library.api.core.ResponseValidator;
import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import automation.library.common.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.json.JSONException;
import org.testng.Assert;
import utils.UniqueValue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ResponseSteps {

    private final RestContext restContext;

    public ResponseSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    private Map<String, Object> valueStore(){
        Map<String, Object> testdata = TestContext.getInstance().testdata();
        if (testdata.containsKey("value_store")){
            return (Map<String, Object>) testdata.get("value_store");
        }else {
            Map<String, Object> valueStore = new HashMap<>();
            testdata.put("value_store", valueStore);
            return valueStore;
        }
    }

    @And("^the raw response body is$")
    public void rawBody(String text){
        String body = restContext.getRestData().getResponse().getBody().asString();
        Assert.assertEquals(body, text, "The Response body does not match");
    }

    @And("the raw response body is {string} from store")
    public void rawBodyFromStore(String key){
        String body = restContext.getRestData().getResponse().getBody().asString();
        Assert.assertEquals(body, valueStore().get(key), "The Response body does not match");
    }

    @And("^the response body contains values from store$")
    @SuppressWarnings("unchecked")
    public void responseBodyValidFromStore(DataTable table) throws IOException, JSONException {
        Map<String, Object> valueStore = valueStore();

        String responseString = restContext.getRestData().getRespString();
        List<ResponseValidator> list = table.asList(ResponseValidator.class);
        list.forEach(responseValidator -> {
            String key = responseValidator.getValue();
            if (valueStore.containsKey(key)){
                responseValidator.setValue(valueStore.get(key).toString());
            }
        });
        RestAssuredHelper.responseContains(list, responseString);
    }

    @And("the raw response body is {string} from store in quotes")
    public void theRawResponseBodyIsFromStoreInQuotes(String key){
        String body = restContext.getRestData().getResponse().getBody().asString();
        String value = valueStore().get(key).toString();
        value = String.format("\"%s\"", value);
        Assert.assertEquals(body, value, "The Response body does not match");
    }

    @Then("the response body contains values and ids from store")
    public void theResponseBodyContainsValuesAndIdsFromStore(DataTable table) throws IOException, JSONException {
        Map<String, Object> valueStore = valueStore();

        String responseString = restContext.getRestData().getRespString();
        List<Map<String,String>> list = table.asMaps();
        List<ResponseValidator> rvList =  list.stream().map(item -> {
            ResponseValidator responseValidator = new ResponseValidator();
            responseValidator.setMatcher(item.get("matcher"));
            responseValidator.setType(item.get("type"));
            String valueKey = item.get("value");
            if (valueStore.containsKey(valueKey)){
                responseValidator.setValue(valueStore.get(valueKey).toString());
            }else {
                responseValidator.setValue(valueKey);
            }

            String id;
            Object rawId = valueStore.get(item.get("id"));
            id = rawId.toString();
            String element = item.get("element").replace("@ID", id);
            responseValidator.setElement(element);

            return responseValidator;
        }).collect(Collectors.toList());
        RestAssuredHelper.responseContains(rvList, responseString);
    }

    @And("the response body contains unique value from date")
    public void theResponseBodyContainsUniqueValueFromDate(DataTable table) throws IOException, JSONException {
        String responseString = restContext.getRestData().getRespString();
        List<ResponseValidator> list = table.asList(ResponseValidator.class);
        list.forEach(responseValidator -> {
            String key = responseValidator.getValue();
            responseValidator.setValue(UniqueValue.uniqueValueFromDate(key));
        });
        RestAssuredHelper.responseContains(list, responseString);
    }
}
