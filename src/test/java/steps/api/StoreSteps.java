package steps.api;

import automation.library.api.core.RestContext;
import automation.library.common.TestContext;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import net.minidev.json.JSONArray;
import org.openqa.selenium.NotFoundException;
import org.testng.Assert;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreSteps {

    private final RestContext restContext;

    public StoreSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> setup(){
        Map<String, Object> valueStore;
        Map<String, Object> testdata = TestContext.getInstance().testdata();
        if (testdata.containsKey("value_store")){
            valueStore = (Map<String, Object>) testdata.get("value_store");
        }else {
            valueStore = new HashMap<>();
            testdata.put("value_store", valueStore);
        }
        return valueStore;
    }

    @And("^store the following values for the request")
    public void storeFromJsonBody(Map<String, String> details){
        Map<String, Object> valueStore = setup();
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        for (Map.Entry<String, String> detail : details.entrySet()){
            Object data = jsonBody.read(detail.getValue());
            valueStore.put(detail.getKey(), data);
        }
    }

    @And("^store the following values for the response$")
    public void storeFromResponse(Map<String, String> details){
        Map<String, Object> valueStore = setup();
        DocumentContext jsonBody = JsonPath.parse(restContext.getRestData().getRespString());
        for (Map.Entry<String, String> detail : details.entrySet()){
            Object data = jsonBody.read(detail.getValue());
            if (data instanceof JSONArray){
                valueStore.put(detail.getKey(), ((JSONArray) data).get(0));
            }else {
                valueStore.put(detail.getKey(), data);
            }
        }

    }

    @And("store the territory for the response")
    public void storeTheTerritoryForTheResponse() {
        Map<String, Object> valueStore = setup();
        DocumentContext jsonBody = JsonPath.parse(restContext.getRestData().getRespString());
        String territory = String.format("%s-%s", jsonBody.read("languageId"), jsonBody.read("countryId"));
        valueStore.put("territory", territory);
    }

    @And("record current sub nodes")
    public void recordCurrentSubNodes(String jsonPath) {
        DocumentContext jsonBody = JsonPath.parse(restContext.getRestData().getRespString());
        List<String> data = jsonBody.read(jsonPath);
        TestContext.getInstance().testdataPut("sub_nodes", data);
    }

    @And("store the new sub nodes as {string}")
    public void storeTheNewSubNodesAs(String key, String jsonPath) {
        Map<String, Object> valueStore = setup();
        DocumentContext jsonBody = JsonPath.parse(restContext.getRestData().getRespString());
        List<String> data = jsonBody.read(jsonPath);
        List<String> olddata = (List<String>) TestContext.getInstance().testdataGet("sub_nodes");

        data.removeAll(olddata);
        Assert.assertEquals(data.size(), 1, "Was expecting only 1 new node");
        valueStore.put(key, data.get(0));
    }

    @And("store the {string} script that was just queued")
    public void storeTheScriptThatWasJustQueued(String scriptName) {
        Instant now = Instant.now();
        Map<String, Object> valueStore = setup();
        DocumentContext jsonBody = JsonPath.parse(restContext.getRestData().getRespString());

        JSONArray data = jsonBody.read("$");

        for (Object index : data){
            Map item = (HashMap<String,Object>) index;
            String script = String.valueOf(item.get("issue_Ref"));
            String queueId = String.valueOf(item.get("id"));
            String textTime = String.valueOf(item.get("dateTimeQueued"));
            Instant time = Instant.parse(textTime + "Z");
            if(script.equals(scriptName) && Duration.between(time, now).toMinutes() < 1L){
                valueStore.put("queueId", queueId);
                return;
            }
        }

        throw new NotFoundException();
    }
}
