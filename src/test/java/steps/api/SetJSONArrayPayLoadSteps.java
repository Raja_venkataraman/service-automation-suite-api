package steps.api;

import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import customFunction.customUtils;
import io.cucumber.java.en.And;

import java.io.FileNotFoundException;

public class SetJSONArrayPayLoadSteps {

    RestContext restContext;

    public SetJSONArrayPayLoadSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @And("a request body contains array of records {string}")
    public void aRequestBodyContainsArrayOfRecords(String data) throws FileNotFoundException {
        String jsonData = customUtils.createJsonArray(data);
        RestAssuredHelper.setBody(restContext.getRestData().getRequest(), jsonData);
    }
}
