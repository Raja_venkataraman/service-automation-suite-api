package steps.api;

import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import automation.library.common.Property;
import automation.library.common.TestContext;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.qameta.allure.Allure;
import utils.UniqueValue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class RequestSteps {

    private final RestContext restContext;

    public RequestSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @Then("^load json body \"(.*)\"$")
    public void loadBody(String file) throws IOException {
        String[] str = file.split("\\.");
        String env = Property.getVariable("cukes.env");
        String path = String.format("src/test/resources/testdata/%s/inputs/%s.json", env , str[0]);
        File baseTestData = new File(path);
        String baseDataSet = str[1];

        DocumentContext wholeJson = JsonPath.parse(baseTestData);
        DocumentContext data = JsonPath.parse(wholeJson.read(baseDataSet, Object.class));
        TestContext.getInstance().testdataPut("json_body", data);
    }

    @And("^in the json body set the following fields to unique values$")
    public void convertToUniqueValue(List<String> paths) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        for (String path : paths){
            String value = jsonBody.read(path).toString();
            jsonBody.set(path, UniqueValue.uniqueValue(value));
        }
    }

    @And("^in the json body set the following fields to unique values from date$")
    public void convertToUniqueValueFromDate(List<String> paths) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        for (String path : paths){
            String value = jsonBody.read(path).toString();
            jsonBody.set(path, UniqueValue.uniqueValueFromDate(value));
        }
    }

    @And("^in the json body set the following fields to stored values$")
    @SuppressWarnings("unchecked")
    public void setFromStore(Map<String, String> details) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        Map<String, Object> valueStore = (Map<String, Object>) TestContext.getInstance().testdataGet("value_store");
        for (Map.Entry<String, String> detail : details.entrySet()){
            jsonBody.set(detail.getKey(), valueStore.get(detail.getValue()));
        }
    }

    @And("^set json body as body$")
    public void addJsonBody() {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        String jsonString = jsonBody.jsonString();
        RestAssuredHelper.setBody(restContext.getRestData().getRequest(), jsonString);
        Allure.getLifecycle().addAttachment("request_body", "application/json", "json", jsonString.getBytes(StandardCharsets.UTF_8));
    }


    @And("in the json body join following fields")
    public void inTheJsonBodyJoinFollowingFields(List<Map<String, String>> details) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        for (Map<String, String> detail : details){
            String input1 = jsonBody.read(detail.get("input1"));
            String input2 = jsonBody.read(detail.get("input2"));
            String output = String.format("%s-%s", input1, input2);
            jsonBody.set(detail.get("output"), output);
        }
    }

    @And("^add file \"(.*)\"$")
    public void addFile(String fileName) throws IOException {
        File file = new File("src/test/resources/testdata/inputs/" + fileName );
        restContext.getRestData().getRequest().multiPart("file", file);
    }

    @And("^set json body as \"(.*)\" multiPart$")
    public void setJsonBodyAsMultiPart(String controlName) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        restContext.getRestData().getRequest().multiPart(controlName,jsonBody.jsonString());
    }

    @Given("^(form parameters|query parameters|path parameters|parameters) from store$")
    public void withParams(String type, Map<String, String> map) {
        Map<String, Object> valueStore = (Map<String, Object>) TestContext.getInstance().testdataGet("value_store");
        StringJoiner joiner = new StringJoiner("\n");
        map.forEach((key, val) -> {
            String value = valueStore.get(val).toString();
            RestAssuredHelper.setParam(restContext.getRestData().getRequest(), type, key, value);
            joiner.add(String.format("%s=%s", key, value));
        });

        Allure.getLifecycle().addAttachment(type, "text/plain", "properties", joiner.toString().getBytes());

    }

    @And("in the json body convert {string} to and unique email from date")
    public void inTheJsonBodyConvertToAndUniqueEmailFromDate(String path) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        Map<String, String> value = jsonBody.read(path);
        String email = String.format("%s%s@%s", value.get("name"), UniqueValue.date(), value.get("domain"));
        jsonBody.set(path, email);
    }

    @And("in the json body set the following fields to uuid")
    public void inTheJsonBodySetTheFollowingFieldsToUuid(List<String> paths) {
        DocumentContext jsonBody = TestContext.getInstance().testdataToClass("json_body", DocumentContext.class);
        for (String path : paths){
            jsonBody.set(path, UniqueValue.uuid());
        }
    }

    @And("set scope")
    public void setScope(List<String> scope) {
        String value = String.join(" ", scope);
        restContext.getRestData().getRequest().formParam("scope", value);
    }

    @And("set scope with env")
    public void setScopeWithEnv(List<String> scopeList) {
        String env = Property.getVariable("cukes.env");
        List<String> scopeListEnv = scopeList.stream().map(scope -> {
            return scope.replace("@ENV", env);
        }).collect(Collectors.toList());
        setScope(scopeListEnv);
    }
}
