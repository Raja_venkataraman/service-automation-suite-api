package steps.api;

import automation.library.api.core.ResponseValidator;
import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import automation.library.common.TestContext;
import customFunction.customUtils;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Allure;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

import static automation.library.common.FileHelper.getJSONData;
import static io.restassured.RestAssured.given;

public class CustomerCustomSteps {
    JSONObject jsonTest = null;

    RestContext restContext;
    RequestSpecification request = given();
    private Object Response;

    public CustomerCustomSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    private String baseTestData = null;
    private String baseDataSet = null;
    private String api = null;
    private String path = null;
    private String method = null;

    public int idValue = 0;
    public String idNameValue = "";

    public void resetRest() {
        request = given();
        restContext.getRestData().setRequest(request);
    }

    @Given("^the request body is \"([^\"]*)\"$")
    public void setBodyInputData(String data) {
        customUtils.setBodyInput(data, restContext.getRestData().getRequest());
    }

    @Given("^the case to be created for Customer from \"([^\"]*)\"$")
    public void setBodyInputDataId(String data) {
        idValue = Integer.parseInt(customUtils.getValueFromJSONFile("outputs", data, "customerId"));
        String firstName = customUtils.getValueFromJSONFile("outputs", data, "firstName");
        String surname = customUtils.getValueFromJSONFile("outputs", data, "surname");
        String fullName = firstName.concat(" ").concat(surname);
        String baseTestData = "src/test/resources/testdata/inputs/newCase.json";
        String baseDataSet = "data1";
        JSONObject jsonObject = getJSONData(baseTestData);
        JSONObject datasetUpdate = getJSONData(baseTestData, baseDataSet);
        datasetUpdate.put("customerId", idValue);
        jsonObject.put(baseDataSet, datasetUpdate);
        customUtils.saveResponse(jsonObject.toString(), "inputs", "newCase");

        baseTestData = "src/test/resources/testdata/outputs/newCase.json";
        baseDataSet = "data1";
        jsonObject = getJSONData(baseTestData);
        datasetUpdate = getJSONData(baseTestData, baseDataSet);
        datasetUpdate.put("customerId", idValue);
        JSONObject customer = datasetUpdate.getJSONObject("customer");
        customer.put("name", fullName);
        datasetUpdate.put("customer", customer);
        baseDataSet = "data2";
        jsonObject = getJSONData(baseTestData);
        datasetUpdate = getJSONData(baseTestData, baseDataSet);
        datasetUpdate.put("customerId", idValue);
        jsonObject.put(baseDataSet, datasetUpdate);
        customUtils.saveResponse(jsonObject.toString(), "outputs", "newCase");
    }

    @Given("^the case to be created for new Customer from \"([^\"]*)\"$")
    public void templateCaseInputAndOutput(String newFileName) {
        idValue = (int) TestContext.getInstance().testdataGet("customerId");
        String baseTestData = "src/test/resources/testdata/inputs/newCase.json";
        String baseDataSet = "data1";
        JSONObject jsonObject = getJSONData(baseTestData);
        JSONObject datasetUpdate = getJSONData(baseTestData, baseDataSet);
        datasetUpdate.put("customerId", idValue);
        jsonObject.put(baseDataSet, datasetUpdate);
        customUtils.saveResponse(jsonObject.toString(), "inputs", newFileName);

        baseTestData = "src/test/resources/testdata/outputs/newCase.json";
        baseDataSet = "data1";
        jsonObject = getJSONData(baseTestData);
        datasetUpdate = getJSONData(baseTestData, baseDataSet);
        datasetUpdate.put("customerId", idValue);
        jsonObject.put(baseDataSet, datasetUpdate);
        customUtils.saveResponse(jsonObject.toString(), "outputs", newFileName);
    }

    @Given("^the request header is \"(.*)\"$")
    public void setHeaderInputData(String data) throws JSONException {
        String[] str = data.split(":");
        RestAssuredHelper.setHeader(restContext.getRestData().getRequest(), str[0], str[1]);
    }

    @Given("^a token header$")
    public void setHeader() {
        String authTokenId = customUtils.getAuthResponse("access_token");
        String val = "Bearer " + authTokenId;
        RestAssuredHelper.setHeader(restContext.getRestData().getRequest(), "Authorization", val);
    }

    @Given("^a invalid token header$")
    public void setInvalidHeader() {
        String authTokenId = customUtils.getAuthResponse("access_token");
        String val = "Bearer " + authTokenId + "x";
        RestAssuredHelper.setHeader(restContext.getRestData().getRequest(), "Authorization", val);
    }

    @Then("^the \"(.*)\" to be updated with \"(.*)\" in file \"([^\"]*)\"$")
    @Given("^the \"(.*)\" to be updated with \"(.*)\" in \"([^\"]*)\"$")
    public void setBodyInputDataUpdate(String id, String filename, String data) {
        idValue = customUtils.getValueFromJSONFile(data, id);

        if (!"titleId".equals(id)) {
            String baseTestData = "src/test/resources/testdata/inputs/" + filename + ".json";
            String baseDataSet = "data1";
            JSONObject jsonObject = getJSONData(baseTestData);
            JSONObject datasetUpdate = getJSONData(baseTestData, baseDataSet);
            datasetUpdate.put(id, idValue);
            jsonObject.put(baseDataSet, datasetUpdate);
            customUtils.saveResponse(jsonObject.toString(), "inputs", filename);
        }
        baseTestData = "src/test/resources/testdata/outputs/" + filename + ".json";
        baseDataSet = "data1";
        JSONObject jsonObject = getJSONData(baseTestData);
        JSONObject datasetUpdate = getJSONData(baseTestData, baseDataSet);
        datasetUpdate.put(id, idValue);
        jsonObject.put(baseDataSet, datasetUpdate);
        customUtils.saveResponse(jsonObject.toString(), "outputs", filename);

    }


    @When("^the test system requests (POST|PATCH) \"(.*)\"$")
    public void apiGetRequest(String apiMethod, String path) {
        this.path = path;
        this.method = apiMethod;
        Response response = RestAssuredHelper.callAPI(restContext.getRestData().getRequest(), apiMethod, path);
        restContext.getRestData().setResponse(response);
    }

    @When("^the test system requests (GET|PUT|DELETE) \"(.*)\"$")
    public void apiGetDeleteUpdateRequest(String apiMethod, String path) {


        if (idValue == 0) {
            path = path + idNameValue;
        } else {
            path = path + idValue;
        }
        this.path = path;
        this.method = apiMethod;
        Response response = RestAssuredHelper.callAPI(restContext.getRestData().getRequest(), apiMethod, path);
        restContext.getRestData().setResponse(response);
        resetRest();       //enables multiple api calls within single scenario
    }

    @And("a response body is returned")
    public void aResponseBodyIsReturned() {
        String type = "json";
        String path = "CaseServices";
        RestAssuredHelper.checkSchema(restContext.getRestData(), type, "testdata/schemas/" + path);
    }

    @And("the response id is captured \"(.*)\"$")
    public void getIdResponse(String Id) {
        idValue = customUtils.getIdFromResponse(restContext.getRestData().getResponse().getBody().asString(),
                Id);
        customUtils.recordResponse(idValue);

    }

    @And("the \"(.*)\" id exists in the response$")
    public void validateResponseIDExists(String Id) {
        idValue = customUtils.getIdFromResponse(restContext.getRestData().getResponse().getBody().asString(),
                Id);
        customUtils.recordResponse(idValue);
        TestContext.getInstance().sa().assertTrue(idValue > 0, String.format("ID(%s) does not exist. Actual: %d", Id, idValue));
    }

    @And("^the \"(.*)\" is be (deleted|updated|retrieved)$")
    public void getIdFromResponse(String Id, String Action, String fileName) {
        idValue = customUtils.getValueFromJSONFile(fileName,
                Id);
        TestContext.getInstance().testdataPut(Id, idValue);
        customUtils.recordResponse(idValue);
    }

    @And("^the \"(.*)\" to be (deleted|updated|retrieved) from file$")
    public void getIdFromFile(String Id, String Action, String fileName) {
        idValue = customUtils.getValueFromJSONFile(fileName,
                Id);
        TestContext.getInstance().testdataPut(Id, idValue);
        customUtils.recordResponse(idValue);
    }

    @And("^the name \"(.*)\" is be (deleted|updated|retrieved)$")
    public void getNameValueFromResponse(String Id, String Action, String fileName) {
        idNameValue = customUtils.getIdNameResponse(fileName,
                Id);
        customUtils.recordResponse(idNameValue);

    }

    @And("^trace out response and attach to report$")
    public void traceOut() {
        System.out.println("Response");
        System.out.println("=====================================================");
        String respBody = restContext.getRestData().getResponse().body().prettyPrint();
        Allure.addAttachment("Response Body", respBody);
    }

    @And("^store the response body$")
    public void storeResponseBodyToFile(String responseFileName) {
        String respBody = restContext.getRestData().getResponse().getBody().asString();
        customUtils.saveResponse(respBody, "outputs", responseFileName);
    }

    @And("^the number of records returned at most are (\\d+)$")
    public void listOfRecordsReturned(int data) {
        Assert.assertEquals(customUtils.getNumberOfRecordsResponse(
                restContext.getRestData().getResponse().body().asString(), data),
                "Number of records is at most " + data);
    }

    @And("^the test (json|response) body( strictly|) contains$")
    public void responseBodyValid(String type, String mode, DataTable table) throws IOException, JSONException {
        List<List<String>> temp = table.cells();
        String responseString = null;
        if (type.equalsIgnoreCase("response")) {
            responseString = restContext.getRestData().getRespString();
        } else {
            responseString = TestContext.getInstance().testdataGet("jsonBody").toString();
        }

        if (temp.get(0).size() == 1) {
            String[] filename = temp.get(0).get(0).replace("<<", "").replace(">>", "").split("\\.");
            customUtils.responseBodyContain(mode, filename[0], filename[1], responseString);
        } else if (temp.get(0).size() == 2) {
            RestAssuredHelper.responseBodyValid(this.api, this.method, this.path, table.asMap(String.class, String.class), responseString);
        } else {
            RestAssuredHelper.responseContains(table.asList(ResponseValidator.class), responseString);
        }
    }

    @And("^the \"(.*)\" is (deleted|updated|retrieved) from input file$")
    public void getIdFromRequest(String Id, String Action, String fileName) {
        idValue = customUtils.getFieldFromInputFile(fileName,
                Id);
        customUtils.recordResponse(idValue);
    }

}
