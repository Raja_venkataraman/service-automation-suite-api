package steps.api;

import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import customFunction.customUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import utils.UniqueValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class RequestParameterSteps {

    RestContext restContext;

    public RequestParameterSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @Given("^(form parameters|query parameters|path parameters|parameters) from (inputs|outputs) file$")
    public void withParamsFromFile(String type, String dir, Map<String, String> map) {
        map.forEach((key, val) -> {
            Boolean list = false;
            List<String> vals = new ArrayList<String>();
            if (val.contains("::")) {
                list = true;
                vals = Arrays.asList(val.split("::"));
            }
            if (val.contains("<<") && val.contains(">>")) {
                val = customUtils.getValueFromJSONFile(dir, val, key);
            }

            if (!list)
                RestAssuredHelper.setParam(restContext.getRestData().getRequest(), type, key, val);
            else
                RestAssuredHelper.setParamList(restContext.getRestData().getRequest(), key, vals);
        });
    }

//    @And("^(form parameters|query parameters|path parameters|parameters) from (inputs|outputs) file$")
//    public void withParamsFrominputoroutputFile(String type, String dir, Map<String, String> map) {
//        map.forEach((key, val) -> {
//            Boolean list = false;
//            List<String> vals = new ArrayList<String>();
//            if (val.contains("::")) {
//                list = true;
//                vals = Arrays.asList(val.split("::"));
//            }
//            if (val.contains("<<") && val.contains(">>")) {
//                val = customUtils.getValueFromJSONFile(dir, val, key);
//            }
//
//            if (!list)
//                RestAssuredHelper.setParam(restContext.getRestData().getRequest(), type, key, val);
//            else
//                RestAssuredHelper.setParamList(restContext.getRestData().getRequest(), key, vals);
//        });
//    }


    @And("^(form parameters|query parameters|path parameters|parameters) with a unique value from date$")
    public void pathParametersWithAUniqueValueFromDate(String type, Map<String, String> map) {
        map.forEach((key, val) -> {
            Boolean list = false;
            List<String> vals = new ArrayList<String>();
            if (val.contains("::")) {
                list = true;
                vals = Arrays.asList(val.split("::"));
            }else {
                val = UniqueValue.uniqueValueFromDate(val);
            }

            if (!list)
                RestAssuredHelper.setParam(restContext.getRestData().getRequest(), type, key, val);
            else
                RestAssuredHelper.setParamList(restContext.getRestData().getRequest(), key, vals);
        });
    }
}
