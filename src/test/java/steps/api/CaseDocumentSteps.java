package steps.api;

import automation.library.api.core.RestContext;
import io.cucumber.java.en.Given;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static utils.APIDownloadHelper.getContentDisposition;

public class CaseDocumentSteps {

    RestContext restContext;
    RequestSpecification request = given();

    public CaseDocumentSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @Given("^the response output file matches input file$")
    public void responseFileMatches(List<String> inputFileName) throws IOException {
        Map<String, String> contentDisposition = getContentDisposition(restContext);
        String expectedFileName = inputFileName.get(0);
        Assert.assertEquals(contentDisposition.get("filename"), expectedFileName, "Expected file does not match");
    }

}
