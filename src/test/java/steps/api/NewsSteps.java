package steps.api;

import automation.library.api.core.RestContext;
import automation.library.common.Property;
import automation.library.common.TestContext;
import automation.library.cucumber.core.Constants;
import customFunction.customUtils;
import io.cucumber.java.en.Given;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import java.util.Map;

import static automation.library.common.FileHelper.getJSONData;
import static io.restassured.RestAssured.given;

public class NewsSteps {

    RestContext restContext;
    RequestSpecification request = given();
    private Object Response;

    public NewsSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    public int idValue = 0;

    public void resetRest() {
        request = given();
        restContext.getRestData().setRequest(request);
    }

    @Given("^the expected news to be templated$")
    public void templateNewsOutput(Map<String, String> sourceDestination) {
        sourceDestination.forEach((from, to) -> {
            idValue = Integer.parseInt(customUtils.getValueFromJSONFile("outputs", from, "newsId"));
            String envPropPath = Constants.ENVIRONMENTPATH + Property.getVariable("cukes.env") + ".properties";
            String clientIDName = (String) TestContext.getInstance().testdataGet("clientIDName");
            String employeeIDKey = clientIDName.concat("_employee_id");
            int employeeId = Integer.parseInt(Property.getProperty(envPropPath, employeeIDKey));

            String baseTestData = "src/test/resources/testdata/outputs/" + to + ".json";
            String baseDataSet = "data1";
            JSONObject jsonObject = getJSONData(baseTestData);
            JSONObject datasetUpdate = getJSONData(baseTestData, baseDataSet);
            datasetUpdate.put("newsId", idValue);
            datasetUpdate.put("employeeId", employeeId);
            jsonObject.put(baseDataSet, datasetUpdate);
            customUtils.saveResponse(jsonObject.toString(), "outputs", to);
        });
    }

}
