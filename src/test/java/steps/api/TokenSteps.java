package steps.api;

import automation.library.api.core.RestAssuredHelper;
import automation.library.api.core.RestContext;
import automation.library.common.Property;
import automation.library.common.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

public class TokenSteps {

    private final RestContext restContext;

    public TokenSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @Then("^set client token")
    public void setClientSecret(){
        String client_secret = Property.getVariable("CLIENT_SECRET");
        RequestSpecification request = restContext.getRestData().getRequest();
        request.formParam("client_secret", client_secret);
    }

    @And("set client {string}")
    public void setClient(String id) {
        String env = Property.getVariable("cukes.env");
        RequestSpecification request = restContext.getRestData().getRequest();
        String client_secret = id + "." + env;
        String client_id = client_secret + ".automation";
        request.formParam("client_id", client_id);
        request.formParam("client_secret", client_secret);
    }

    @Then("^store \"(.*)\" as a token$")
    public void storeToken(String path){
        JsonPath jsonPath = new JsonPath(restContext.getRestData().getResponse().getBody().asString());
        String token = jsonPath.get(path);
        TestContext.getInstance().testdataPut("token", token);
    }

    @Then("^set token header$")
    public void setHeader() {
        String authTokenId = TestContext.getInstance().testdataToClass("token", String.class);
        String val = "Bearer " + authTokenId;
        RestAssuredHelper.setHeader(restContext.getRestData().getRequest(), "Authorization", val);
    }
}
