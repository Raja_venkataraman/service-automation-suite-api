package steps.api;

import automation.library.api.core.RestContext;
import customFunction.RestAssuredHelperExtension;
import io.cucumber.java.en.Given;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class MultiPartFormSteps {

    RestContext restContext;
    RequestSpecification request = given();

    public MultiPartFormSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @Given("^a multipart form$")
    public void givenMultiPartForm(Map<String, String> formFields) {
        if (formFields.containsKey("File")) {
            RestAssuredHelperExtension.setMultiPartForm(restContext.getRestData().getRequest(), formFields.get("File"));
        }

    }
}
