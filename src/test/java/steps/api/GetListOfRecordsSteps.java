package steps.api;

import automation.library.api.core.RestContext;
import io.cucumber.java.en.And;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class GetListOfRecordsSteps {

    RestContext restContext;

    public GetListOfRecordsSteps(RestContext restContext) {
        this.restContext = restContext;
    }

    @And("the response body contains more than {int} records")
    public void theResponseBodyContainsMoreThanRecords(int expectedNumber) {
        JSONArray jsonArray = new JSONArray(restContext.getRestData().getResponse().getBody().asString());
        int numberOfRecordsFromResponse = jsonArray.length();
        Assert.assertTrue(numberOfRecordsFromResponse > expectedNumber, +numberOfRecordsFromResponse + " Records existing which is more than expectedNumber");
    }

    @And("the response body (.*) contains more than (\\d+) records")
    public void theResponseBodyElementContainsMoreThanRecords(String element, int expectedNumber) {
        JSONObject body = new JSONObject(restContext.getRestData().getResponse().getBody().asString());
        JSONArray elementArray = (JSONArray) body.get(element);
        int numberOfRecordsFromResponse = elementArray.length();
        String msg = String.format("%d Records existing which is less than expectedNumber. ",
                numberOfRecordsFromResponse);
        Assert.assertTrue(numberOfRecordsFromResponse >= expectedNumber, msg);
    }
}
